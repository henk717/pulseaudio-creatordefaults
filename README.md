# pulseaudio-creatordefaults

A default.pa configuration file that can be used as a template for content creators to get a creator friendly pulse audio setup with audio normalization, microphone preview and a application to microphone relay for easy audio routing.

## Installation
Copy the bundled .so files to /usr/lib/ladspa, then place default.pa in ~/.config/pulse and restart your system (pulseaudio -k can be used as a test but may cause other pulseaudio configurators not to function).

By default we assume that the correct physical speaker operates at sink 0 and the correct microphone operates at sink 1.
If this is not correct for your system try reloading pulseaudio first with everything plugged in by using pulseaudio -k, and then use pacmd list-sources and pacmd list-sinks and adjust the sink=0 and source=1 values accordingly to the number displayed in the index field from output of these commands.

## Usage
The configuration can be used in many different ways but for optimal usage we recommend the following :
- Do not adjust the volume of the Virtual-OUT devices, leave them at 100% and instead adjust the volume of the Volume-IN devices. This will ensure that the volume is identical to what you perceive in your own headset or speakers.
- Leave Virtual-OUT-MIX at 100% volume to mimic the sound balance of the other inputs correctly, only make adjustments to the Virtual-IN and/or other Virtual-OUT devices (And of course your own physical devices).
- Do not use Virtual-IN-Local for editing software, this will compensate for editing mistakes in your own headset/speakers making editing errors hard to hear.
- Set applications that you use to consume content to Virtual-IN-Local to enjoy system wide sound improvements.
- Use pavucontrol to easily change the settings per application.
- Do not use desktop capture in your software, instead of use Virtual-OUT-APP or Virtual-OUT-MIX and route the specific programs you want to capture to Virtual-APP-IN. This will give you normalized volume across the entire product and ensures no unexpected programs or sounds are recorded.
- Adjust the capture volume of your microphone so it still holds the maximum volume but captures as little noise or background noise as possible. The config will amply uncancelled background noise, this is by design because your listeners would always hear this if they have loudness equalization on on there systems.

### Recording in OBS
This pulseaudio configuration will add some audio enhancements you can use with OBS to increase your studio productivity and quality. Because of the audio normalization you will spend less time adjusting the correct volume balances for your microphone and you will have realtime easy to hear volume balancing using your normal system controls.

For optimal usage disable the Desktop Audio capture and set the microphone capture to Virtual-OUT-MIX. By default OBS will consider the volume of the configuration to loud, you can achieve a quieter non red soundbar by adjusting the Virtual-OUT-MIC and Virtual-OUT-APP to 75% and then adjusting the volumes with the Virtual-IN-MIC and Virtual-IN-APP or alternatively setting the Virtual-IN-MIC and Virtual-IN-APP to 75% and adjusting the volumes using the Virtual-OUT-MIC and Virtual-OUT-APP depending on your preferences.

If you like to use more advanced tricks such as dynamic music volume depending on the slide you are on (For example to do a music fade on the logo) we recommend muting the Mic/Aux input of OBS and creating Audio Input Capture separate sources for Virtual-IN-APP and Virtual-IN-MIC with the volumes you need. This way you can have a Virtual-IN-APP source that plays at 50% volume on one slide with a Virtual-IN-MIC microphone at 100% while transitioning to a slide with a 100% volume Virtual-IN-APP source where the microphone is not present.

### Using the pulseaudio config in podcasts or livestreams with multiple participants
To use the configuration effectively in broadcasts where there are multiple participants it is important to filter the participants from hearing themselves. To do this use the Virtual-IN-APP device as your conference audio source and Virtual-OUT-MIC as your recording device for the conferencing software.
You can then use Virtual-OUT-MIX in your recording or streaming program and use the system volume controls to achieve a desirable audio balance. Because the normalization is done on both the incoming audio and your own microphone you will achieve near equal volume for all participants in the podcast or stream.

### Using the configuration to route music to your discord friends / achieve normalized audio
Set your discord microphone to Virtual-OUT-MIX, turn automatic gain control off, disable all the quality enhancement options in discord. Now use pavucontrol to route your music player of choice to Virtual-IN-APP and adjust the Virtual-IN-APP volume to the desired level. You will now be able to speak and listen to music with friends at the same time.

## Known issues
The delay on the virtual adapters can increase significantly over time when exposed to system suspention or high CPU loads.
This can be fixed by adding max_latency_msec= to the module-loopback's, but requires pulseaudio 13 to function which as of 1-10-2018 is not yet in the repository's.
